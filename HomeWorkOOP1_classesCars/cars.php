<?php

class Cars
{
    public $color, $mark, $model, $hp;

    public function set_color($color)
    {
        $this->color = $color;
        return $this;
    }

    public function set_mark($mark)
    {
        $this->mark = $mark;
        return $this;

    }

    public function set_model($model)
    {
        $this->model = $model;
        return $this;

    }

    public function set_hp($hp)
    {
        $this->hp = $hp;
        return $this;

    }

    public function get_color()
    {
        return $this->color;
    }

    public function get_mark()
    {
        return $this->mark;
    }

    public function get_model()
    {
        return $this->model;
    }

    public function get_hp()
    {
        return $this->hp;
    }

    public function get_info()
    {
        echo "Function getInfo Class Cars: <br>";
        $properties = ["mark", "model", "color", "hp"];
        $res = [];
        foreach ($properties as $prop) {
            $res[] = $this->{"get_" . $prop}();
        }
        return implode(", ", $res);
    }

};

class PetrolCars extends Cars {



    static $petrolType;

    public function set_petrolType($petrol)
    {
        $this->pertolType = $petrol;
        return $this;

    }

    public function get_petrolType()
    {
        return $this->pertolType;
    }


    public function get_info()
    {
        echo "Function getInfo Class PetrolCars: <br>";
        return (parent::get_info().", ".$this->get_petrolType());

    }

}

class CrossoverPetrolCars extends PetrolCars {



    static $crossoverType;

    public function set_crossoverType($crossoverType)
    {
        $this->crossoverType = $crossoverType;
        return $this;

    }

    public function get_crossoverType()
    {
        return $this->crossoverType;
    }


    public function get_info()
    {
        echo "Function getInfo Class CrossoverPetrolCars: <br>";
        return (parent::get_info().", ".$this->get_crossoverType());

    }

}


$fordTransit = new Cars;
$fordTransit->set_color("White")
    ->set_mark("Ford")
    ->set_model("Transit")
    ->set_hp(100);
echo $fordTransit->get_info();
echo "<br><br><br>";

$kiaSportage = new PetrolCars();
$kiaSportage->set_color("Grey")
    ->set_mark("Kia")
    ->set_model("Sportage")
    ->set_hp(101)
    ->set_petrolType("Disel");
echo $kiaSportage->get_info();
echo "<br><br><br>";

$fordFlex = new CrossoverPetrolCars();
$fordFlex->set_color("Black")
    ->set_mark("Ford")
    ->set_model("Flex")
    ->set_hp(171)
    ->set_petrolType("A95")
    ->set_crossoverType("full-size crossover");
echo $fordFlex->get_info();
echo "<br><br><br>";
