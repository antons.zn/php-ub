1. Создаем дополнительную таблицу genres.
create table genres(id int(3) primary key not null auto_increment, genre varchar(40) );

2. Заполняем таблицу  genres
insert into genres (genre) values ('detective'),('horror'),('comedy'),('art-hause'),('action'),('drama');

3. Добавляем и переименовываем столбец в таблице Films , поле genre_id 
alter table films add genre int(3);
alter table films change genre genre_id int(3);

4. Добавляем в таблицу Фильмы жанры по ID
update films set genre = 1 where id=5;
update films set genre = 4 where id=2;
update films set genre = 6 where id>=3 and id<=4;

5. Создаем таблицу filmes_actors (id_film, id_actor)
create table film_actor (id_film int(3), id_actor int(3));

6. Создаем таблицу films_directors (id, id_director)
create table films_directors ( id int(3) primary key not null,  director_id int(3) );

Практикуем запросы:
а) вывести все ДРАМЫ из базы фильмов.

SELECT title, genre 
FROM films f 
INNER JOIN genres g 
ON f.genre_id=g.id 
WHERE genre="drama";

б) вывести всех актеров, кто играл в “Shawshank redemption”

SELECT name, surname 
FROM ((films 
INNER JOIN film_actor ON (films.id = film_actor.id_film)) 
INNER JOIN actors ON (actors.id=film_actor.id_actor)) 
WHERE title="Shawshank redemption";


в) вывести всех актеров, которые сыграли в Drama

SELECT DISTINCT name, surname 
FROM (((films 
INNER JOIN film_actor ON films.id = film_actor.id_film) 
INNER JOIN actors ON actors.id=film_actor.id_actor) 
INNER JOIN genres ON genres.id=films.genre_id) 
WHERE genre="drama";

г) вывести в скольких Drama сыграл Tom Hanks;

SELECT DISTINCT COUNT(*) as “Tom Hanks dramas” 
FROM (((films f 
INNER JOIN film_actor fa ON f.id = fa.id_film) 
INNER JOIN actors a ON a.id=fa.id_actor) 
INNER JOIN genres g ON g.id=f.genre_id) 
WHERE genre="drama" 
GROUP BY name="Tom" and surname="Hanks" ;

д) вывести все фильмы и их всех внесенных Режисеров.

SELECT films.title AS 'title of film', d.surname AS 'director surname' 
FROM ((films 
LEFT JOIN films_directors fd on films.id = fd.id) 
LEFT JOIN directors d ON d.id = fd.director_id) 
ORDER BY title;

e) показать режисеров фильма Матрица используя USING
SELECT films.title AS 'title of film', d.surname AS 'director surname' 
FROM ((films  LEFT JOIN films_directors fd USING(id)) 
LEFT JOIN directors d ON d.id =
fd.director_id) 
WHERE title = 'Matrix';

ж) пробуем FOREIGN KEY
4_just_practive_foreign_key.png


6. Дерево.
Создаем таблицу родословной семьи Ланистеров

create table lanisters (
    -> id int(2) not null primary key,
    -> parent_id int(2),
    -> name varchar(40)
    -> );
    
наполняем ее:

insert to lanisters (id, parent_id, name) values (1, 4, “Joffrey”);
…
mysql> select * from lanisters;
...

Задание: 
Показать родовое дерево “Joffrey” Lanister.

select l4.name as name, l3.name as "son of" , l2.name as "son of" , l1.name as "son of" from lanisters l1 inner join lanisters l2 on (l1.id = l2.parent_id) inner join lanisters l3 on (l2.id=l3.parent_id) inner join lanisters l4 on (l3.id=l4.parent_id) where l4.name = 'Joffrey';
3__family_tree.jpg

