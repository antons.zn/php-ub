<?php
//Создать несколько объектов существующих классов,
// реализовать полиморфизм так, как вы его понимаете.
//Разработать структуру классов для одного из перечисленных вариантов:
//мотоциклы, скутеры, квадроциклы
//Реализовать изученные принципы, проверить соблюдение принципов SOLID,
// прокомментировать их соблюдение в коде в комментариях.

////L - Liskov substitution principle (объект класса родителя можно было бы без нарушения функционала
///  заменить на объект класса потомка)
interface QuantityWheels
{
    public function set_QuantityWheels($QuantityWheels);

    public function get_QuantityWheels();
}

//S - Single responsibility principle - принцип единой ответственности
interface ShowTrip
{
    public function show_trip($object);
}


////L - Liskov substitution principle (объект класса родителя можно было бы без нарушения функционала
///  заменить на объект класса потомка)
abstract class Vehicle implements QuantityWheels
{
    abstract function parking();

//O - Open/Closed principle - принцип открытости/закрытости.
    protected $QuantityWheels = 4;

    public function start()
    {
        echo "I've started my way on my " . get_class($this);
        echo "<br>";
        return $this;
    }

    public function stop()
    {
        echo "I've stopped my " . get_class($this);
        echo "<br>";
        return $this;
    }

//O - Open/Closed principle - принцип открытости/закрытости.
    public function set_QuantityWheels($size)
    {
        $this->wheelSize = $size;
        return $this;
    }

//O - Open/Closed principle - принцип открытости/закрытости.
    public function get_QuantityWheels()
    {
        return $this->wheelSize;
    }


}

//O - Open/Closed principle - принцип открытости/закрытости. Суть принципа - класс должен быть закрыт для изменений,
// но открыт для расширения в классах-наследниках.
class Bike extends Vehicle
{

    public function parking()
    {
        echo "I've found parking for bicycles and parked my  " . get_class($this);
        echo "<br>";
    }


}

//O - Open/Closed principle - принцип открытости/закрытости. Суть принципа - класс должен быть закрыт для изменений,
// но открыт для расширения в классах-наследниках.
class Scooter extends Vehicle
{
    public function parking()
    {
        echo "Parking. I could take my " . get_class($this) . " with me. I didn't need parking place";
        echo "<br>";
    }

}

//O - Open/Closed principle - принцип открытости/закрытости. Суть принципа - класс должен быть закрыт для изменений,
// но открыт для расширения в классах-наследниках.
class QuadBike extends Vehicle
{
    public function parking()
    {
        echo "I had a standard size of " . get_parent_class($this) . ", so I needed a big parking place for my " . get_class($this);
        echo "<br>";
    }

}

//S - Single responsibility principle - принцип единой ответственности.
//    I - Interface Segregation principle
class People implements ShowTrip
{
    public function __construct($name)
    {
        $this->name = $name;
    }

    protected $name;

    public function set_name($name)
    {
        $this->name = $name;
        return $this;
    }

    public function get_name()
    {
        return $this->name;
    }
//D - Dependency Inversion principle - принцип инверсии зависимости.
// Полиморфизм
    public function show_trip($obj)
    {
        $obj->start();
        $obj->parking();
        $obj->stop();
    }

}

$viecles = [
    $bike = new Bike,
    $scooter = new Scooter,
    $QuadBike = new QuadBike
];


$vasya = new People("Vasya");

$rand = rand(0, 2);
if ($rand == 0) $transp = "Bike";
if ($rand == 1) $transp = "Scooter";
if ($rand == 2) $transp = "QuadBike";

echo "<b>Story about Vasya <br><br> Today:</b> <br>";
$vasya->show_trip($viecles[$rand]);
if ($viecles[$rand] != 1)
    $viecles[$rand]->set_QuantityWheels($rand + 2);
else
    $viecles[$rand]->set_QuantityWheels(2);
;

echo "By the way, the transport " . get_class($viecles[$rand]) . " has " . $viecles[$rand]->get_QuantityWheels() . " wheels";
echo "<br><br>";
